import streamlit as st
import pandas as pd
from catboost import CatBoostRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import numpy as np


@st.cache()
def get_data():
    df_s = pd.read_csv('flights.csv')
    return df_s

df = get_data()
df_f1 = df.drop(['CANCELLATION_REASON', 'AIR_SYSTEM_DELAY', 'SECURITY_DELAY', 'AIRLINE_DELAY', 'LATE_AIRCRAFT_DELAY', 'WEATHER_DELAY'],axis=1)
df_new = df_f1[df_f1['MONTH'] == 5]
df_f1.dropna(inplace=True)
df_new['DATE'] = pd.to_datetime(df_new[['YEAR','MONTH', 'DAY']])


st.header('Аэропорт вылета')
diapozon = df['ORIGIN_AIRPORT']
selected_test_size = st.selectbox("Выбрать аэропорт вылета", options=diapozon)
df_new ['ORIGIN_AIRPORT'] = selected_test_size


if st.button('Создать модель'):
    X_train, X_test, y_train, y_test = train_test_split(df_new .drop('ARRIVAL_DELAY', axis=1), df_new ['ARRIVAL_DELAY'], test_size= 25,
                                                        random_state=0)
    st.text('Размер данных-'+str(X_train.shape) + str( X_test.shape))

    st.text('Старт модели')
    model = CatBoostRegressor()
    model.fit(X_train, y_train)
    st.text('Обучили модель')
    pred = model.predict(X_test)
    st.write(pd.DataFrame({'Real':y_test,'Predict':pred}).reset_index(drop=True))